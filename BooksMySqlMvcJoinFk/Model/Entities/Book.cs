﻿using System;

namespace BooksMySqlMvcJoinFk.Model.Entities
{
    class Book
    {
        public int ArticleCd { get; set; }
        public string Title { get; set; }
        public int AuthorCd { get; set; }
        public int PublicationDate { get; set; }
        public DateTime DateOfReceipt { get; set; }

        public Author Author { get; set; }
    }
}
