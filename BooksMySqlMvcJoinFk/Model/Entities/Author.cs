﻿using System;

namespace BooksMySqlMvcJoinFk.Model.Entities
{
    class Author
    {
        public int AuthorCd { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
