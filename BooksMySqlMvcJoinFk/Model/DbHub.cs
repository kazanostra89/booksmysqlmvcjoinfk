﻿using BooksMySqlMvcJoinFk.Model.Entities;

namespace BooksMySqlMvcJoinFk.Model
{
    static class DbHub
    {
        private static object hub;

        static DbHub()
        {
            hub = null;
        }

        public static void Reset()
        {
            hub = null;
        }


        public static object Hub
        {
            get
            {
                return hub;
            }

            set
            {
                if (value is Book || value is Author)
                {
                    hub = value;
                }
                else
                {
                    hub = null;
                }
            }
        }

    }
}
