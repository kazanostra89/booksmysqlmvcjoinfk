﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using BooksMySqlMvcJoinFk.Model.Tables;
using BooksMySqlMvcJoinFk.Model.Tools;

namespace BooksMySqlMvcJoinFk.Model
{
    class DbManager
    {
        private static DbManager dbManager = null;
        private MySqlConnection conSql;

        private TableAuthors tableAuthors;
        private TableBooks tableBooks;

        private DbManager()
        {
            conSql = ConnectionManager.GetConnection();
            conSql.Open();

            tableAuthors = new TableAuthors(conSql);
            tableBooks = new TableBooks(conSql, tableAuthors);
        }

        ~DbManager()
        {
            conSql.Close();
        }

        public static DbManager GetDbManager()
        {
            if (dbManager == null)
            {
                dbManager = new DbManager();
            }

            return dbManager;
        }

        public void CheckingConnectionSql()
        {
            if (conSql.State == ConnectionState.Closed)
            {
                conSql.Open();
            }
        }


        public TableAuthors TableAuthors
        {
            get { return tableAuthors; }
        }

        public TableBooks TableBooks
        {
            get { return tableBooks; }
        }

    }
}
