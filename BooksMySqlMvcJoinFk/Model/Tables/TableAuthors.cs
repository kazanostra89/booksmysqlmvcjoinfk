﻿using System;
using System.Collections.Generic;
using BooksMySqlMvcJoinFk.Model.Entities;
using MySql.Data.MySqlClient;

namespace BooksMySqlMvcJoinFk.Model.Tables
{
    class TableAuthors
    {
        private List<Author> authors;
        private MySqlConnection conSql;

        public TableAuthors(MySqlConnection conSql)
        {
            authors = new List<Author>();
            this.conSql = conSql;

            LoadAuthors();
        }

        private void LoadAuthors()
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "SELECT * FROM authors;";

                using (MySqlDataReader reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        authors.Add(new Author()
                        {
                            AuthorCd = reader.GetInt32("AuthorCD"),
                            Name = reader.GetString("Name")
                        });
                    }
                }
            }
        }

        public void InsertNewAuthor(Author author)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "INSERT INTO authors (Name) VALUES (@name);";

                mySqlCommand.Parameters.AddWithValue("@name", author.Name);

                int affectedRows = mySqlCommand.ExecuteNonQuery();

                if (affectedRows == 1)
                {
                    mySqlCommand.CommandText = "SELECT LAST_INSERT_ID();";

                    author.AuthorCd = Convert.ToInt32(mySqlCommand.ExecuteScalar());

                    authors.Add(author);
                }
            }
        }

        public void DeleteAuthorByAuthorCd(int authorCd)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "DELETE FROM authors WHERE AuthorCD = @authorCd;";
                mySqlCommand.Parameters.AddWithValue("@authorCd", authorCd);

                int affectedRows = mySqlCommand.ExecuteNonQuery();

                if (affectedRows == 1)
                {
                    authors.Remove(authors.Find(item => item.AuthorCd == authorCd));
                }
            }
        }

        public void UpdateAuthorByAuthorCd(Author author)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "UPDATE authors SET Name=@name WHERE AuthorCD = @authorCd;";

                mySqlCommand.Parameters.AddWithValue("@authorCd", author.AuthorCd);
                mySqlCommand.Parameters.AddWithValue("@name", author.Name);

                int affectedRows = mySqlCommand.ExecuteNonQuery();

                if (affectedRows == 1)
                {
                    authors.Find(item => item.AuthorCd == author.AuthorCd).Name = author.Name;
                }
            }
        }


        public List<Author> Rows
        {
            get { return authors; }
        }

    }
}
