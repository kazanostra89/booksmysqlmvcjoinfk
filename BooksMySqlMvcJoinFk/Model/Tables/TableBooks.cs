﻿using System;
using System.Collections.Generic;
using BooksMySqlMvcJoinFk.Model.Entities;
using MySql.Data.MySqlClient;

namespace BooksMySqlMvcJoinFk.Model.Tables
{
    class TableBooks
    {
        private List<Book> books;
        private MySqlConnection conSql;

        public TableBooks(MySqlConnection conSql, TableAuthors tableAuthors)
        {
            books = new List<Book>();
            this.conSql = conSql;

            LoadBooks(tableAuthors);
        }

        private void LoadBooks(TableAuthors tableAuthors)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "SELECT * FROM books;";

                using (MySqlDataReader reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        books.Add(new Book()
                        {
                            ArticleCd = reader.GetInt32("ArticleCD"),
                            Title = reader.GetString("Title"),
                            AuthorCd = reader.GetInt32("AuthorCD"),
                            PublicationDate = reader.GetInt32("PublicationDate"),
                            DateOfReceipt = reader.GetDateTime("DateOfReceipt"),

                            Author = tableAuthors.Rows.Find(item => item.AuthorCd == reader.GetInt32("AuthorCD"))
                        });
                    }
                }
            }
        }

        public void DeleteByArticleCd(int articleCd)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "DELETE FROM books WHERE ArticleCD = @articleCd;";
                mySqlCommand.Parameters.AddWithValue("@articleCd", articleCd);

                int affectedRows = mySqlCommand.ExecuteNonQuery();

                if (affectedRows == 1)
                {
                    books.Remove(books.Find(item => item.ArticleCd == articleCd));
                }
            }
        }

        public void InsertNewBook(Book book)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "INSERT INTO books (Title, AuthorCD, PublicationDate, DateOfReceipt) VALUES (@title, @authorCd, @publicationDate, @dateOfReceipt);";

                mySqlCommand.Parameters.AddWithValue("@title", book.Title);
                mySqlCommand.Parameters.AddWithValue("@authorCd", book.AuthorCd);
                mySqlCommand.Parameters.AddWithValue("@publicationDate", book.PublicationDate);
                mySqlCommand.Parameters.AddWithValue("@dateOfReceipt", book.DateOfReceipt);

                int affectedRows = mySqlCommand.ExecuteNonQuery();

                if (affectedRows == 1)
                {
                    mySqlCommand.CommandText = "SELECT LAST_INSERT_ID();";

                    book.ArticleCd = Convert.ToInt32(mySqlCommand.ExecuteScalar());

                    books.Add(book);
                }
            }
        }

        public void UpdateByArticleCd(Book book)
        {
            using (MySqlCommand mySqlCommand = conSql.CreateCommand())
            {
                mySqlCommand.CommandText = "UPDATE books SET Title=@title, AuthorCD=@authorCD, PublicationDate=@publicationDate, DateOfReceipt=@dateOfReceipt WHERE ArticleCD = @articleCd;";

                mySqlCommand.Parameters.AddWithValue("@articleCd", book.ArticleCd);
                mySqlCommand.Parameters.AddWithValue("@title", book.Title);
                mySqlCommand.Parameters.AddWithValue("@authorCd", book.AuthorCd);
                mySqlCommand.Parameters.AddWithValue("@publicationDate", book.PublicationDate);
                mySqlCommand.Parameters.AddWithValue("@dateOfReceipt", book.DateOfReceipt);

                int affectedRows = mySqlCommand.ExecuteNonQuery();

                if (affectedRows == 1)
                {
                    Book updBook = books.Find(item => item.ArticleCd == book.ArticleCd);

                    updBook.Title = book.Title;
                    updBook.AuthorCd = book.AuthorCd;
                    updBook.PublicationDate = book.PublicationDate;
                    updBook.DateOfReceipt = book.DateOfReceipt;
                    updBook.Author = book.Author;
                }
            }
        }


        public List<Book> Rows
        {
            get { return books; }
        }


    }
}
