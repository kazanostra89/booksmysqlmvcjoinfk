﻿using System;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Views.Authors;
using BooksMySqlMvcJoinFk.Model;
using BooksMySqlMvcJoinFk.Model.Entities;

namespace BooksMySqlMvcJoinFk.Controllers.Authors
{
    class ControllerFormUpdate
    {
        private FormUpdate fUpdate;
        private DbManager db;

        public ControllerFormUpdate(FormUpdate fUpdate)
        {
            this.fUpdate = fUpdate;
            db = DbManager.GetDbManager();
        }

        public void FillingInAllFields()
        {
            if (DbHub.Hub != null && DbHub.Hub is Author)
            {
                fUpdate.textBoxNameAuthor.Text = ((Author)DbHub.Hub).Name;
            }
            else
            {
                fUpdate.buttonUpdateAuthor.Visible = false;
            }
        }

        public void UpdateAuthor()
        {
            if (fUpdate.textBoxNameAuthor.TextLength != 0)
            {
                Author author = new Author()
                {
                    AuthorCd = ((Author)DbHub.Hub).AuthorCd,
                    Name = fUpdate.textBoxNameAuthor.Text
                };
                
                try
                {
                    db.CheckingConnectionSql();

                    db.TableAuthors.UpdateAuthorByAuthorCd(author);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

                DbHub.Reset();
                fUpdate.Close();
            }
            else
            {
                MessageBox.Show("Не заполнено поле - Автор");
            }
        }

        public void CloseForm()
        {
            DbHub.Reset();
            fUpdate.Close();
        }

    }
}
