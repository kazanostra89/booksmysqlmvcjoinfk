﻿using System;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Views.Authors;
using BooksMySqlMvcJoinFk.Model;
using BooksMySqlMvcJoinFk.Model.Entities;

namespace BooksMySqlMvcJoinFk.Controllers.Authors
{
    class ControllerFormInsert
    {
        private FormInsert fInsert;
        private DbManager db;

        public ControllerFormInsert(FormInsert fInsert)
        {
            this.fInsert = fInsert;
            db = DbManager.GetDbManager();
        }

        public void InsertAuthor()
        {
            if (fInsert.textBoxNameAuthor.TextLength != 0)
            {
                Author author = new Author()
                {
                    Name = fInsert.textBoxNameAuthor.Text
                };
                
                try
                {
                    db.CheckingConnectionSql();

                    db.TableAuthors.InsertNewAuthor(author);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

                fInsert.Close();
            }
            else
            {
                MessageBox.Show("Не заполнено поле - Автор");
            }
        }

        public void CloseForm()
        {
            fInsert.Close();
        }

    }
}
