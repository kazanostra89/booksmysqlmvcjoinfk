﻿using System;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Views.Authors;
using BooksMySqlMvcJoinFk.Model;

namespace BooksMySqlMvcJoinFk.Controllers.Authors
{
    class ControllerFormMain
    {
        private FormaMain fMain;
        private DbManager db;

        public ControllerFormMain(FormaMain fMain)
        {
            this.fMain = fMain;
            db = DbManager.GetDbManager();
        }

        public void UpdateDataGridViewAuthors()
        {
            fMain.dataGridViewAuthors.DataSource = null;
            fMain.dataGridViewAuthors.DataSource = db.TableAuthors.Rows;

            fMain.dataGridViewAuthors.Columns["AuthorCd"].Visible = false;
            fMain.dataGridViewAuthors.Columns["Name"].HeaderText = "Автор";

            fMain.dataGridViewAuthors.ClearSelection();
        }

        public void DeleteTheSelectedAuthor()
        {
            if (fMain.dataGridViewAuthors.SelectedRows.Count != 0)
            {
                int authorCd = (int)fMain.dataGridViewAuthors.SelectedRows[0].Cells["AuthorCd"].Value;
               
                try
                {
                    db.CheckingConnectionSql();

                    db.TableAuthors.DeleteAuthorByAuthorCd(authorCd);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            else
            {
                MessageBox.Show("Выберите автора для удаления!");
            }
        }

        public void OpenFormInsert()
        {
            new FormInsert().ShowDialog();
        }

        public void OpenFormUpdate()
        {
            if (fMain.dataGridViewAuthors.SelectedRows.Count != 0)
            {
                DbHub.Hub = fMain.dataGridViewAuthors.SelectedRows[0].DataBoundItem;

                new FormUpdate().ShowDialog();
            }
            else
            {
                MessageBox.Show("Выберите автора для редактирования!");
            }
        }


    }
}
