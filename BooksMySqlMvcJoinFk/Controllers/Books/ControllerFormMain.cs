﻿using System;
using System.Data;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Model;
using BooksMySqlMvcJoinFk.Views.Books;

namespace BooksMySqlMvcJoinFk.Controllers.Books
{
    class ControllerFormMain
    {
        private FormMain fMain;
        private DbManager db;

        public ControllerFormMain(FormMain formMain)
        {
            fMain = formMain;

            try
            {
                db = DbManager.GetDbManager();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void UpdateDataGridViewBooks()
        {
            fMain.dataGridViewBooks.DataSource = null;

            try
            {
                fMain.dataGridViewBooks.DataSource = db.TableBooks.Rows;

                fMain.dataGridViewBooks.Columns["ArticleCd"].Visible = false;
                fMain.dataGridViewBooks.Columns["AuthorCd"].Visible = false;

                fMain.dataGridViewBooks.Columns["Title"].HeaderText = "Название";
                fMain.dataGridViewBooks.Columns["PublicationDate"].HeaderText = "Дата публикации";
                fMain.dataGridViewBooks.Columns["DateOfReceipt"].HeaderText = "Дата поступления";
                fMain.dataGridViewBooks.Columns["Author"].HeaderText = "Автор";

                fMain.dataGridViewBooks.Columns["Title"].DisplayIndex = 0;
                fMain.dataGridViewBooks.Columns["Author"].DisplayIndex = 1;
                fMain.dataGridViewBooks.Columns["PublicationDate"].DisplayIndex = 2;
                fMain.dataGridViewBooks.Columns["DateOfReceipt"].DisplayIndex = 3;

                fMain.dataGridViewBooks.Columns["Author"].Width = 220;
                fMain.dataGridViewBooks.Columns["PublicationDate"].Width = 80;
                fMain.dataGridViewBooks.Columns["DateOfReceipt"].Width = 110;

                fMain.dataGridViewBooks.ClearSelection();
            }
            catch
            {
                MessageBox.Show("Нет соединения с базой данных!\nПриложение будет закрыто!");

                Application.Exit();
            }
        }

        public void DeleteTheSelectedBook()
        {
            if (fMain.dataGridViewBooks.SelectedRows.Count != 0)
            {
                int articleCd = (int)fMain.dataGridViewBooks.SelectedRows[0].Cells["ArticleCd"].Value;
                
                try
                {
                    db.CheckingConnectionSql();

                    db.TableBooks.DeleteByArticleCd(articleCd);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            else
            {
                MessageBox.Show("Не выбрана книга для удаления");
            }
        }

        public void OpenFormInsert()
        {
            new FormInsert().ShowDialog();
        }

        public void OpenFormUpdate()
        {
            if (fMain.dataGridViewBooks.SelectedRows.Count != 0)
            {
                DbHub.Hub = fMain.dataGridViewBooks.SelectedRows[0].DataBoundItem;

                new FormUpdate().ShowDialog();
            }
            else
            {
                MessageBox.Show("Не выбрана книга для редактирования");
            }
        }

        public void OpenFormMainAuthor()
        {
            new BooksMySqlMvcJoinFk.Views.Authors.FormaMain().ShowDialog();
        }
        

    }
}
