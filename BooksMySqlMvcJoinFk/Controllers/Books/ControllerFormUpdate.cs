﻿using System;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Views.Books;
using BooksMySqlMvcJoinFk.Model;
using BooksMySqlMvcJoinFk.Model.Entities;

namespace BooksMySqlMvcJoinFk.Controllers.Books
{
    class ControllerFormUpdate
    {
        private FormUpdate fUpdate;
        private DbManager db;

        public ControllerFormUpdate(FormUpdate fUpdate)
        {
            this.fUpdate = fUpdate;
            db = DbManager.GetDbManager();
        }

        public void FillingInAllFields()
        {
            fUpdate.comboBoxAuthor.DataSource = db.TableAuthors.Rows;

            if (DbHub.Hub != null && DbHub.Hub is Book)
            {
                Book hubBook = (Book)DbHub.Hub;

                fUpdate.textBoxTitle.Text = hubBook.Title;
                fUpdate.textBoxPublicationDate.Text = hubBook.PublicationDate.ToString();
                fUpdate.dateTimePickerDateOfReceipt.Value = hubBook.DateOfReceipt;
                fUpdate.comboBoxAuthor.SelectedItem = hubBook.Author;
            }
            else
            {
                fUpdate.buttonUpdate.Visible = false;
            }
        }

        public void UpdateBook()
        {
            if (fUpdate.textBoxTitle.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Название");
                return;
            }
            if (fUpdate.textBoxPublicationDate.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Дата публикации");
                return;
            }

            Book updateBook = new Book()
            {
                ArticleCd = ((Book)DbHub.Hub).ArticleCd,
                Title = fUpdate.textBoxTitle.Text,
                AuthorCd = ((Author)fUpdate.comboBoxAuthor.SelectedItem).AuthorCd,
                PublicationDate = Convert.ToInt32(fUpdate.textBoxPublicationDate.Text),
                DateOfReceipt = fUpdate.dateTimePickerDateOfReceipt.Value.Date,
                Author = (Author)fUpdate.comboBoxAuthor.SelectedItem
            };
            
            try
            {
                db.CheckingConnectionSql();

                db.TableBooks.UpdateByArticleCd(updateBook);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            DbHub.Reset();
            fUpdate.Close();
        }

        public void CloseForm()
        {
            DbHub.Reset();
            fUpdate.Close();
        }


    }
}
