﻿using System;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Model;
using BooksMySqlMvcJoinFk.Views.Books;
using BooksMySqlMvcJoinFk.Model.Entities;

namespace BooksMySqlMvcJoinFk.Controllers.Books
{
    class ControllerFormInsert
    {
        private FormInsert fInsert;
        private DbManager db;

        public ControllerFormInsert(FormInsert formInsert)
        {
            fInsert = formInsert;
            db = DbManager.GetDbManager();
        }

        public void LoadingTheListOfAuthors()
        {
            fInsert.comboBoxAuthor.DataSource = null;
            fInsert.comboBoxAuthor.DataSource = db.TableAuthors.Rows;

            fInsert.dateTimePickerDateOfReceipt.Value = DateTime.Now;
        }

        public void InsertBook()
        {
            if (CheckTheFillingOfFields())
            {
                Book newBook = new Book()
                {
                    Title = fInsert.textBoxTitle.Text,
                    AuthorCd = ((Author)fInsert.comboBoxAuthor.SelectedItem).AuthorCd,
                    PublicationDate = Convert.ToInt32(fInsert.textBoxPublicationDate.Text),
                    DateOfReceipt = fInsert.dateTimePickerDateOfReceipt.Value.Date,
                    Author = (Author)fInsert.comboBoxAuthor.SelectedItem
                };
                
                try
                {
                    db.CheckingConnectionSql();

                    db.TableBooks.InsertNewBook(newBook);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

                fInsert.Close();
            }
        }

        private bool CheckTheFillingOfFields()
        {
            if (fInsert.textBoxTitle.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Название");
                fInsert.textBoxTitle.Focus();
                return false;
            }
            else if (fInsert.textBoxPublicationDate.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Дата публикации");
                fInsert.textBoxPublicationDate.Focus();
                return false;
            }

            return true;
        }

        public void CloseFrom()
        {
            fInsert.Close();
        }

    }
}
