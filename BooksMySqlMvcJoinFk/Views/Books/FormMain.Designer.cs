﻿namespace BooksMySqlMvcJoinFk.Views.Books
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewBooks = new System.Windows.Forms.DataGridView();
            this.buttonGetAllBooks = new System.Windows.Forms.Button();
            this.buttonInsertBook = new System.Windows.Forms.Button();
            this.buttonDeleteBook = new System.Windows.Forms.Button();
            this.buttonEditBook = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAuthors = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBooks)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewBooks
            // 
            this.dataGridViewBooks.AllowUserToAddRows = false;
            this.dataGridViewBooks.AllowUserToDeleteRows = false;
            this.dataGridViewBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBooks.Location = new System.Drawing.Point(8, 48);
            this.dataGridViewBooks.Margin = new System.Windows.Forms.Padding(5);
            this.dataGridViewBooks.MultiSelect = false;
            this.dataGridViewBooks.Name = "dataGridViewBooks";
            this.dataGridViewBooks.ReadOnly = true;
            this.dataGridViewBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBooks.Size = new System.Drawing.Size(745, 256);
            this.dataGridViewBooks.TabIndex = 0;
            // 
            // buttonGetAllBooks
            // 
            this.buttonGetAllBooks.Location = new System.Drawing.Point(576, 312);
            this.buttonGetAllBooks.Name = "buttonGetAllBooks";
            this.buttonGetAllBooks.Size = new System.Drawing.Size(176, 56);
            this.buttonGetAllBooks.TabIndex = 1;
            this.buttonGetAllBooks.Text = "Обновить список всех книг";
            this.buttonGetAllBooks.UseVisualStyleBackColor = true;
            this.buttonGetAllBooks.Click += new System.EventHandler(this.buttonUpdateAll_Click);
            // 
            // buttonInsertBook
            // 
            this.buttonInsertBook.Location = new System.Drawing.Point(8, 320);
            this.buttonInsertBook.Name = "buttonInsertBook";
            this.buttonInsertBook.Size = new System.Drawing.Size(144, 32);
            this.buttonInsertBook.TabIndex = 2;
            this.buttonInsertBook.Text = "Добавить книгу";
            this.buttonInsertBook.UseVisualStyleBackColor = true;
            this.buttonInsertBook.Click += new System.EventHandler(this.buttonInsertBook_Click);
            // 
            // buttonDeleteBook
            // 
            this.buttonDeleteBook.Location = new System.Drawing.Point(344, 320);
            this.buttonDeleteBook.Name = "buttonDeleteBook";
            this.buttonDeleteBook.Size = new System.Drawing.Size(144, 32);
            this.buttonDeleteBook.TabIndex = 3;
            this.buttonDeleteBook.Text = "Удалить книгу";
            this.buttonDeleteBook.UseVisualStyleBackColor = true;
            this.buttonDeleteBook.Click += new System.EventHandler(this.buttonDeleteBook_Click);
            // 
            // buttonEditBook
            // 
            this.buttonEditBook.Location = new System.Drawing.Point(176, 320);
            this.buttonEditBook.Name = "buttonEditBook";
            this.buttonEditBook.Size = new System.Drawing.Size(144, 32);
            this.buttonEditBook.TabIndex = 4;
            this.buttonEditBook.Text = "Изменить книгу";
            this.buttonEditBook.UseVisualStyleBackColor = true;
            this.buttonEditBook.Click += new System.EventHandler(this.buttonEditBook_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(320, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Список книг";
            // 
            // buttonAuthors
            // 
            this.buttonAuthors.Location = new System.Drawing.Point(576, 376);
            this.buttonAuthors.Name = "buttonAuthors";
            this.buttonAuthors.Size = new System.Drawing.Size(176, 56);
            this.buttonAuthors.TabIndex = 6;
            this.buttonAuthors.Text = "Просмотреть список всех авторов";
            this.buttonAuthors.UseVisualStyleBackColor = true;
            this.buttonAuthors.Click += new System.EventHandler(this.buttonAuthors_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 443);
            this.Controls.Add(this.buttonAuthors);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEditBook);
            this.Controls.Add(this.buttonDeleteBook);
            this.Controls.Add(this.buttonInsertBook);
            this.Controls.Add(this.buttonGetAllBooks);
            this.Controls.Add(this.dataGridViewBooks);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "БИБЛИОТЕКА";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBooks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewBooks;
        private System.Windows.Forms.Button buttonGetAllBooks;
        private System.Windows.Forms.Button buttonInsertBook;
        private System.Windows.Forms.Button buttonDeleteBook;
        private System.Windows.Forms.Button buttonEditBook;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAuthors;
    }
}