﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Controllers.Books;

namespace BooksMySqlMvcJoinFk.Views.Books
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;

        public FormMain()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormMain(this);
        }

        private void buttonUpdateAll_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewBooks();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewBooks();
        }

        private void buttonDeleteBook_Click(object sender, EventArgs e)
        {
            controller.DeleteTheSelectedBook();

            controller.UpdateDataGridViewBooks();
        }

        private void buttonInsertBook_Click(object sender, EventArgs e)
        {
            controller.OpenFormInsert();

            controller.UpdateDataGridViewBooks();
        }

        private void buttonEditBook_Click(object sender, EventArgs e)
        {
            controller.OpenFormUpdate();

            controller.UpdateDataGridViewBooks();
        }

        private void buttonAuthors_Click(object sender, EventArgs e)
        {
            controller.OpenFormMainAuthor();

            controller.UpdateDataGridViewBooks();
        }
    }
}
