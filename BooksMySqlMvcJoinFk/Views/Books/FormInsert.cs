﻿using System;
using BooksMySqlMvcJoinFk.Controllers.Books;
using System.Windows.Forms;

namespace BooksMySqlMvcJoinFk.Views.Books
{
    public partial class FormInsert : Form
    {
        private ControllerFormInsert controller;

        public FormInsert()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormInsert(this);
        }

        private void FormInsert_Load(object sender, EventArgs e)
        {
            controller.LoadingTheListOfAuthors();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseFrom();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            controller.InsertBook();
        }

        private void textBoxPublicationDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)48 && e.KeyChar <= 57 && textBoxPublicationDate.TextLength < 4) || e.KeyChar == (char)Keys.Back)
            {
                if (e.KeyChar == (char)Keys.D0 && textBoxPublicationDate.TextLength == 0)
                {
                    e.Handled = true;
                }
                
                return;
            }

            e.Handled = true;
        }

        private void textBoxPublicationDate_TextChanged(object sender, EventArgs e)
        {
            textBoxPublicationDate.Text = textBoxPublicationDate.Text.TrimStart((char)Keys.D0);
        }
    }
}
