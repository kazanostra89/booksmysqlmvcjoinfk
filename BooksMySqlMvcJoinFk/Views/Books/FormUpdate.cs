﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Controllers.Books;

namespace BooksMySqlMvcJoinFk.Views.Books
{
    public partial class FormUpdate : Form
    {
        private ControllerFormUpdate controller;

        public FormUpdate()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormUpdate(this);
        }

        private void FormUpdate_Load(object sender, EventArgs e)
        {
            controller.FillingInAllFields();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            controller.UpdateBook();
        }
    }
}
