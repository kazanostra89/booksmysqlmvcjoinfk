﻿namespace BooksMySqlMvcJoinFk.Views.Authors
{
    partial class FormaMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewAuthors = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonGetAllAuthors = new System.Windows.Forms.Button();
            this.buttonInsertAuthor = new System.Windows.Forms.Button();
            this.buttonUpdateAuthor = new System.Windows.Forms.Button();
            this.buttonDeleteAuthor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAuthors)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewAuthors
            // 
            this.dataGridViewAuthors.AllowUserToAddRows = false;
            this.dataGridViewAuthors.AllowUserToDeleteRows = false;
            this.dataGridViewAuthors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAuthors.Location = new System.Drawing.Point(8, 32);
            this.dataGridViewAuthors.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewAuthors.MultiSelect = false;
            this.dataGridViewAuthors.Name = "dataGridViewAuthors";
            this.dataGridViewAuthors.ReadOnly = true;
            this.dataGridViewAuthors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAuthors.Size = new System.Drawing.Size(336, 184);
            this.dataGridViewAuthors.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Авторы";
            // 
            // buttonGetAllAuthors
            // 
            this.buttonGetAllAuthors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonGetAllAuthors.Location = new System.Drawing.Point(8, 232);
            this.buttonGetAllAuthors.Name = "buttonGetAllAuthors";
            this.buttonGetAllAuthors.Size = new System.Drawing.Size(336, 24);
            this.buttonGetAllAuthors.TabIndex = 2;
            this.buttonGetAllAuthors.Text = "Обновить список авторов";
            this.buttonGetAllAuthors.UseVisualStyleBackColor = true;
            this.buttonGetAllAuthors.Click += new System.EventHandler(this.buttonGetAllAuthors_Click);
            // 
            // buttonInsertAuthor
            // 
            this.buttonInsertAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonInsertAuthor.Location = new System.Drawing.Point(8, 264);
            this.buttonInsertAuthor.Name = "buttonInsertAuthor";
            this.buttonInsertAuthor.Size = new System.Drawing.Size(336, 24);
            this.buttonInsertAuthor.TabIndex = 3;
            this.buttonInsertAuthor.Text = "Добавить автора";
            this.buttonInsertAuthor.UseVisualStyleBackColor = true;
            this.buttonInsertAuthor.Click += new System.EventHandler(this.buttonInsertAuthor_Click);
            // 
            // buttonUpdateAuthor
            // 
            this.buttonUpdateAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonUpdateAuthor.Location = new System.Drawing.Point(8, 296);
            this.buttonUpdateAuthor.Name = "buttonUpdateAuthor";
            this.buttonUpdateAuthor.Size = new System.Drawing.Size(336, 24);
            this.buttonUpdateAuthor.TabIndex = 4;
            this.buttonUpdateAuthor.Text = "Изменить";
            this.buttonUpdateAuthor.UseVisualStyleBackColor = true;
            this.buttonUpdateAuthor.Click += new System.EventHandler(this.buttonUpdateAuthor_Click);
            // 
            // buttonDeleteAuthor
            // 
            this.buttonDeleteAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDeleteAuthor.Location = new System.Drawing.Point(8, 328);
            this.buttonDeleteAuthor.Name = "buttonDeleteAuthor";
            this.buttonDeleteAuthor.Size = new System.Drawing.Size(336, 24);
            this.buttonDeleteAuthor.TabIndex = 5;
            this.buttonDeleteAuthor.Text = "Удалить";
            this.buttonDeleteAuthor.UseVisualStyleBackColor = true;
            this.buttonDeleteAuthor.Click += new System.EventHandler(this.buttonDeleteAuthor_Click);
            // 
            // FormaMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 362);
            this.Controls.Add(this.buttonDeleteAuthor);
            this.Controls.Add(this.buttonUpdateAuthor);
            this.Controls.Add(this.buttonInsertAuthor);
            this.Controls.Add(this.buttonGetAllAuthors);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewAuthors);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FormaMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторы";
            this.Load += new System.EventHandler(this.FormaMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAuthors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonGetAllAuthors;
        private System.Windows.Forms.Button buttonInsertAuthor;
        private System.Windows.Forms.Button buttonUpdateAuthor;
        private System.Windows.Forms.Button buttonDeleteAuthor;
        public System.Windows.Forms.DataGridView dataGridViewAuthors;
    }
}