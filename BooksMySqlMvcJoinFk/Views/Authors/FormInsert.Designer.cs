﻿namespace BooksMySqlMvcJoinFk.Views.Authors
{
    partial class FormInsert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            this.textBoxNameAuthor = new System.Windows.Forms.TextBox();
            this.buttonInsertAuthor = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 16);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(51, 16);
            label1.TabIndex = 0;
            label1.Text = "Автор:";
            // 
            // textBoxNameAuthor
            // 
            this.textBoxNameAuthor.Location = new System.Drawing.Point(8, 40);
            this.textBoxNameAuthor.Name = "textBoxNameAuthor";
            this.textBoxNameAuthor.Size = new System.Drawing.Size(320, 22);
            this.textBoxNameAuthor.TabIndex = 1;
            // 
            // buttonInsertAuthor
            // 
            this.buttonInsertAuthor.Location = new System.Drawing.Point(8, 80);
            this.buttonInsertAuthor.Name = "buttonInsertAuthor";
            this.buttonInsertAuthor.Size = new System.Drawing.Size(104, 32);
            this.buttonInsertAuthor.TabIndex = 2;
            this.buttonInsertAuthor.Text = "Добавить";
            this.buttonInsertAuthor.UseVisualStyleBackColor = true;
            this.buttonInsertAuthor.Click += new System.EventHandler(this.buttonInsertAuthor_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(224, 80);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(104, 32);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // FormInsert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 122);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonInsertAuthor);
            this.Controls.Add(this.textBoxNameAuthor);
            this.Controls.Add(label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "FormInsert";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление автора";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonInsertAuthor;
        private System.Windows.Forms.Button buttonCancel;
        public System.Windows.Forms.TextBox textBoxNameAuthor;
    }
}