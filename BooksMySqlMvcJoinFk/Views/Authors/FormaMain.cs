﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Controllers.Authors;

namespace BooksMySqlMvcJoinFk.Views.Authors
{
    public partial class FormaMain : Form
    {
        private ControllerFormMain controller;

        public FormaMain()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormMain(this);
        }

        private void FormaMain_Load(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewAuthors();
        }

        private void buttonGetAllAuthors_Click(object sender, EventArgs e)
        {
            controller.UpdateDataGridViewAuthors();
        }

        private void buttonInsertAuthor_Click(object sender, EventArgs e)
        {
            controller.OpenFormInsert();

            controller.UpdateDataGridViewAuthors();
        }

        private void buttonUpdateAuthor_Click(object sender, EventArgs e)
        {
            controller.OpenFormUpdate();

            controller.UpdateDataGridViewAuthors();
        }

        private void buttonDeleteAuthor_Click(object sender, EventArgs e)
        {
            controller.DeleteTheSelectedAuthor();

            controller.UpdateDataGridViewAuthors();
        }
    }
}
