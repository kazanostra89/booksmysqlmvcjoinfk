﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BooksMySqlMvcJoinFk.Controllers.Authors;

namespace BooksMySqlMvcJoinFk.Views.Authors
{
    public partial class FormInsert : Form
    {
        private ControllerFormInsert controller;

        public FormInsert()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerFormInsert(this);
        }

        private void buttonInsertAuthor_Click(object sender, EventArgs e)
        {
            controller.InsertAuthor();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            controller.CloseForm();
        }
    }
}
